import os
import datetime

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


def provide_path(path):
    try:
        os.makedirs(path);
    except:
        pass


def find_nth(str, substr, n):
    parts= str.split(substr, n + 1)
    if len(parts) <= n+1:
        return -1
    return len(str) - len(parts[-1]) - len(substr)


def get_images_list(path, path_template, quantity):
    img_list = []
    c = 0
    for f in os.listdir(path):
        if c >= quantity:
            return img_list
        if f.endswith('.jpg') or f.endswith('.jpeg') or f.endswith('.png'):
            img_list.append(path_template+'/'+f)
            c += 1
    return img_list


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def log_message(msg):
    message = '\n' + str(datetime.datetime.today()) + '\t' + msg.replace('\n', '\n\t\t')
    log_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../', 'app.log')
    log_file_path = log_file_path
    log_file = open(log_file_path, 'a')
    log_file.write(message)
    log_file.close()