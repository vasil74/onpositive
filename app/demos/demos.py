import json
import os
import imageio
import datetime
from werkzeug.utils import secure_filename

from app import app
from app import tools as t
from app.demos import definitions

DEMOS = None


class InputParameter:
    def __init__(self, param_type: str, label: str, name: str, is_visible: bool = True, preset_value=None, description: str = ''):
        self.param_type = param_type
        self.label = label
        self.name = name
        self.preset_value = preset_value
        self.description = description
        self.is_visible = is_visible


class OutputParameter:
    def __init__(self, param_type: str, label: str, name: str, is_visible: bool = True, description: str = ''):
        self.param_type = param_type
        self.label = label
        self.name = name
        self.description = description
        self.is_visible = is_visible


class DemoItem:
    def __init__(self, demo_type, demo_id, predictor, input_params: [InputParameter], output_params: [OutputParameter], title: str='', description: str=''):
        self.demo_type = demo_type
        self.demo_id = demo_id
        self.predictor = predictor
        self.title = title
        self.description = description
        self.input_params = input_params
        self.output_params = output_params

    def get_input_values(self, request):
        input_params = {}
        for input_param in self.input_params:
            if input_param.param_type == 'submit':
                pass
            elif input_param.param_type == 'image':
                content = request.form[input_param.name]
                if len(content) > 0:
                    start_pos = t.find_nth(content, '/', 2)
                    sample_img = os.path.join(app.root_path, 'static', content[start_pos + 1:])
                    file_content = imageio.imread(sample_img)
                    filename = content[content.rindex('/')+1:]
                    pfn = str(datetime.datetime.now()) + '_' + filename
                    t.provide_path(app.config['UPLOAD_FOLDER'])
                    o_file = os.path.join(app.config['UPLOAD_FOLDER'], pfn)
                    imageio.imwrite(o_file, file_content)
                    input_params[input_param.name] = pfn

            elif len(request.files) > 0 and input_param.param_type == 'file':
                file = request.files[input_param.name]
                filename = secure_filename(file.filename)
                if file and t.allowed_file(file.filename):
                    file_content = imageio.imread(file)
                    t.provide_path(app.config['UPLOAD_FOLDER'])
                    pfn = str(datetime.datetime.now()) + '_' + filename
                    o_file = os.path.join(app.config['UPLOAD_FOLDER'], pfn)
                    imageio.imwrite(o_file, file_content)
                    input_params[input_param.name] = pfn
            else:
                input_params[input_param.name] = request.form[input_param.name]
        return input_params

    def render_demo(self, request):
        if len(self.input_params) + len(self.output_params) == 0:
            return ''

        input_params_values = {}
        output_params_values = {}
        if request.method == 'POST':
            input_params_values = self.get_input_values(request)
            if len(input_params_values) > 0:
                output_params_values = self.predictor.predict(input_params_values)

        input_params: {str: str} = {'form': '', 'body': '', 'image': ''}
        for i_param in self.input_params:
            if input_params_values.__contains__(i_param.name) and i_param.param_type != 'image':
                self.render_parameter(i_param, input_params, input_params_values[i_param.name])
            else:
                self.render_parameter(i_param, input_params)
        i_images = ''
        if len(input_params['image']) > 0:
            i_images = ' \n<div class="img-examples">' +\
                            input_params['image'] +\
                       ' \n</div>'

        output_params: {str: str} = {'form': '','body': '', 'image': ''}
        if len(output_params_values)>0:
            for o_param in self.output_params:
                if output_params_values.__contains__(o_param.name):
                    self.render_parameter(o_param, output_params, output_params_values[o_param.name])
                else:
                    self.render_parameter(o_param, output_params)
        o_images = ''
        if len(output_params_values)>0 and len(output_params['image']) > 0:
            o_images = ' \n<div class="img-examples">' + \
                            output_params['image'] + \
                       ' \n</div>'

        demo_html: str = \
            '<div> \n ' \
                '<h1>'+self.title+'</h1> \n ' \
                '<div class="description"><h3>'+self.description+'</h3></div> \n ' \
                '<form name="uploadForm" id="uploadForm" method=post enctype=multipart/form-data onsubmit="return validateUpload(name)" > \n' \
                    + input_params['form'] + \
                '\n</form>' + \
                    o_images + \
                    i_images +\
                    input_params['body'] +\
                    output_params['body'] + \
            '</div>'

        return demo_html

    def render_parameter(self, param, layout:{str:str}, param_value=''):
        if param.param_type == 'file':
            layout['form'] += '<h5>'+param.description+'</h5>'\
                              ' \n<input type=file name="'+param.name+'" id="'+param.name+'">'

        elif param.param_type == 'input_text':
            layout['form'] += '<h5>'+param.description+'</h5>'\
                              ' \n<p>'+param.label+'<br>'\
                              ' \n<input type="text" id="' + param.name + '" size="80" name="' + param.name + '" value="'+param.preset_value+'" style="display: block">'

        elif param.param_type == 'output_text':
            layout['body'] += '<h5>'+param.description+'</h5>'\
                              ' \n<p>' + param.label + '<br>'\
                              ' \n<div class="output_text" id="' + param.name + '" name="' + param.name + '">'+param_value+'</div >'

        elif param.param_type == 'submit':
            layout['form'] += ' \n<div class="btn_container"><br><input class="btn" type=submit value="'+param.label+'">' \
                              ' \n<div class="loader" style="display:none"></div></div>'

        elif param.param_type == 'image' and param_value is not '':
            display = 'none'
            if param.is_visible:
                display = 'block'
            layout['image'] += ' \n<img name="'+param.name+'" src="'+param_value+'" alt="'+param.label+'" style="display: '+display+'" onclick="send_image(this)">'
            layout['form'] += ' \n<input type="text" id="'+param.name+'" name="'+param.name+'" style="display: none">'

        elif param.param_type == 'image':
            display = 'none'
            if param.is_visible:
                display = 'block'
            layout['image'] += ' \n<img name="'+param.name+'" src="'+param.preset_value+'" alt="'+param.label+'" style="display: '+display+'" onclick="send_image(this)">'
            layout['form'] += ' \n<input type="text" id="' + param.name + '" name="' + param.name + '" style="display: none">'

        elif param.param_type == 'checkbox':
            layout['form'] += '<h5>'+param.description+'</h5> \n<input type=checkbox name="'+param.name+'" + '+param_value+'> ' + param.label + '<br>'

        elif param.param_type == 'textarea':
            layout['form'] += '<h5>'+param.description+'</h5>'\
                              ' \n<p><b>'+param.label+'</b></p>' \
                              ' \n<p><textarea name="'+param.name+'" >'+param_value+'</textarea></p>'
        if param.param_type == 'dropdown_textarea':
            dropdown_content = ''
            for item in param.preset_value.split('<br>'):
                dropdown_content += '\n\t\t<a onclick="set_example(this, \''+param.name+'\')">'+item+'</a>'
            layout['form'] += '<h5>'+param.description+'</h5>'\
                                ' \n<div class="dropdown">'\
                                  '\n\t<button class="dropbtn" onclick="return false;" onmouseover="show_examples(\''+param.name+'_examples\', \'block\')">'+param.label+'</button>'\
                                  '\n\t<div class="dropdown-content" id="'+param.name+'_examples" onmouseleave="show_examples(\''+param.name+'_examples\', \'none\')">'\
                                        +dropdown_content+\
                                  '\n\t</div>'\
                                '\n</div>' \
                                '\n<br>' \
                                '\n<textarea name="' + param.name + '" id="'+param.name+'">'+param_value+'</textarea>'


class Demos:
    def __init__(self, demo_items):
        self.demo_items = demo_items

    def get_demo_by_id(self, demo_id):
        if self.demo_items.__contains__(demo_id):
            return self.demo_items[demo_id]
        else:
            return None


class DemoManager:
    def __init__(self, data_file = './demos_data.json'):
        global DEMOS
        if DEMOS is None:
            demo_items = self.load_demos(data_file)
            DEMOS = Demos(demo_items)
        self.demos = DEMOS

    def load_demos(self, data_file):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../', data_file)
        demos_data = {}
        demos = {}
        try:
            with open(path, 'r') as settings_json:
                demos_data = json.loads(settings_json.read())
        except Exception as e:
            t.log_message('DemoManager.load_demos Exception on settings load: ' + str(e))
            return demos
        for k in demos_data['demos']:
            demo = demos_data['demos'][k]
            demo_item = DemoItem(
                    demo_type=demo['demo_type'],
                    demo_id=str(k),
                    predictor=definitions.PREDICTORS[k],
                    input_params=self.load_input_parameters(demo['input_params']),
                    output_params=self.load_output_parameters(demo['output_params']),
                    title=demo['title'],
                    description=demo['description'])
            demos[k] = demo_item
        return demos

    def load_input_parameters(self, params):
        input_params = []
        for param in params:
            i_param = InputParameter(
                param_type=param['param_type'],
                label=param['label'],
                name=param['name'],
                is_visible=param['is_visible'],
                preset_value=param['preset_value'],
                description=param['description']
            )
            input_params.append(i_param)
        return input_params

    def load_output_parameters(self, params):
        output_params = []
        for param in params:
            o_param = OutputParameter(
                param_type=param['param_type'],
                label=param['label'],
                name=param['name'],
                is_visible=param['is_visible'],
                description=param['description']
            )
            output_params.append(o_param)
        return output_params
