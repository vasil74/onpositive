from flair.data import Sentence
from flair.models import SequenceTagger

class NerPredictorEng:

    def __init__(self):
        self.tagger = SequenceTagger.load('ner')

    def predict(self, text):
        sentence = Sentence(text)
        self.tagger.predict(sentence)
        predictions = []
        sentence_to_dict = sentence.to_dict('ner')
        entities = sentence_to_dict['entities']
        for i in range(len(sentence)):
            try:
                token = sentence.get_token(i+1)
                start_pos_token = token.start_position
                end_pos_token = token.end_position
                in_entities = False
                for entity in entities:
                    if entity['start_pos'] == start_pos_token:
                        predictions.append((entity['text'], entity['type']))
                        in_entities = True
                    elif entity['end_pos'] == end_pos_token:
                        in_entities = True
                if not in_entities:
                    predictions.append((token.text, 'O'))
            except:
                break
        return predictions


def predict(input_params):
    output_params = {}
    predictions = NerPredictorEng().predict(input_params['text'])
    predicted_text = ''
    for token in predictions:
        if 'LOC' in token[1]:
            predicted_text += """<div class="tooltip"><span style="color:Goldenrod;">%s</span><span class="tooltiptext" style="background-color: Goldenrod">Location</span></div> """ % (token[0])
        elif 'ORG' in token[1]:
            predicted_text += """<div class="tooltip"><span style="color:Brown;">%s</span><span class="tooltiptext" style="background-color: Brown">Organization</span></div> """ % (token[0])
        elif 'PER' in token[1]:
            predicted_text += """<div class="tooltip"><span style="color:OliveDrab;">%s</span><span class="tooltiptext" style="background-color: OliveDrab">Person</span></div> """ % (token[0])
        else:
            predicted_text += """%s""" % (token[0] + ' ')
    output_params['predicted_text'] = predicted_text
    return output_params