from app.demos import picsart as p
from app.demos import sentiment_demo_eng as sde
from app.demos import ner_eng as nere
from app.demos import ner_rus as nerrus
from app.demos import topics_predictor_eng as tpe
from app.demos import topics_predictor_rus as tpr

PREDICTORS = {
    'picsart': p,
    'sentiment_demo_eng': sde,
    'ner_eng': nere,
    'ner_rus': nerrus,
    'tpe': tpe,
    'tpr': tpr
}