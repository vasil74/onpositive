import imageio
import imgaug
import os

import numpy as np

from segmentation_pipeline import segmentation

from app import app

MODEL=None


class Predictor:

    def __init__(self, fold=0, stage=0, data = './app/static/data/demos/picsart'):
        global MODEL
        cfg = segmentation.parse(data + "/people.yaml")
        if MODEL is None:
            self.mdl = cfg.load_model(fold, stage)
            MODEL=self.mdl
        else:
            self.mdl=MODEL
        self.ta = cfg.transformAugmentor()

    def predict_file(self, i_file, o_file):
        content = imageio.imread(i_file)
        self.predict_to_file(o_file, content)
        pass

    def predict_to_file(self, o_file, i_content=None, ttflips=False):
        if i_content is None:
            i_content = imageio.imread(o_file)
        ai = self.ta.augment_image(i_content)
        data = np.array([ai])
        res = self.mdl.predict(data)
        if ttflips:
            res = self.flips(res, [ai])
        map = imgaug.SegmentationMapOnImage(res[0], res[0].shape)
        scaledMap = imgaug.augmenters.Scale({"height": i_content.shape[0], "width": i_content.shape[1]}).augment_segmentation_maps([map])
        imageio.imwrite(o_file,
                        imgaug.HeatmapsOnImage(scaledMap[0].arr, scaledMap[0].arr.shape).draw_on_image(i_content)[0])

    def flips(self, res, images_aug):
        another = imgaug.augmenters.Fliplr(1.0).augment_images(images_aug);
        res1 = self.mdl.predict(np.array(another))
        res1 = imgaug.augmenters.Fliplr(1.0).augment_images(res1)

        another1 = imgaug.augmenters.Flipud(1.0).augment_images(images_aug);
        res2 = self.mdl.predict(np.array(another1))
        res2 = imgaug.augmenters.Flipud(1.0).augment_images(res2)

        seq = imgaug.augmenters.Sequential([imgaug.augmenters.Fliplr(1.0), imgaug.augmenters.Flipud(1.0)])
        another2 = seq.augment_images(images_aug);
        res3 = self.mdl.predict(np.array(another2))
        res3 = seq.augment_images(res3)

        res = (res + res1 + res2 + res3) / 4.0
        return res


def predict(input_params):
    output_params = {}
    pfn = ''
    for p in input_params:
        if len(input_params[p])>0:
            pfn = input_params[p]
            o_file = os.path.join(app.config['UPLOAD_FOLDER'], pfn)
            Predictor().predict_to_file(o_file=o_file)
    output_params['result_image'] = pfn
    return output_params
