function validateUpload(formName) {
    var hasData = false;
    for (var i = 0; i<$('#'+formName).children().length; i++){
        var itemType = $('#'+formName).children().get(i).type
        if ((itemType === "file" | itemType === "textarea" | itemType === "text") && $('#'+formName).children().get(i).value.length > 0){
            hasData = true;
            break;
        }
    }
    if (hasData){
        $('.loader').attr('style', 'display: inline-block');
        return true;
    } else{
        alert("Data expected, but field is empty. ");
        return false
    }

}

$(document).ready(function(event){
    $("#fileUploadForm").on('submit', function(event) {
        event.preventDefault();
        var $form = $( this );
        var url = $form.attr( 'action' );
        $("#result_image").attr('style', 'display:none');
        $('.loader').attr('style', 'display:block');
        $.ajax({
            type: 'POST',
            url: url,
            data: new FormData(this),
            mimeType: 'multipart/form-data',
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
                var responseObj = JSON.parse(response)
                $('.loader').attr('style', 'display:none')
                // if (response['result']=='ok'){
                $('#'+$form.attr('id'))[0].reset();
                $("#result_image").attr('src', responseObj['result_image'])
                $("#result_image").attr('style', 'display: block')
                // }
            },
            error: function(error) {
                $('.loader').attr('style', 'display:none')
            }
        });
        return false;
    });
});

function send_image_ajax(img) {
    document.getElementById('hContent').value = img.src;
    $("#result_image").attr('style', 'display:none');
    $('.loader').attr('style', 'display:block');
    $.ajax({
        url:     $("#fileUploadForm").attr( 'action' ),
        type:     "POST",
        dataType: "html",
        data: $("#fileUploadForm").serialize(),
        success: function(response) {
            var responseObj = JSON.parse(response)
            $('.loader').attr('style', 'display:none')
            // if (response['result']=='ok'){
            $('#fileUploadForm')[0].reset();
            $("#result_image").attr('src', responseObj['result_image']);
            $("#result_image").attr('style', 'display: block')
            // }
        },
        error: function(response) {
            $('.loader').attr('style', 'display:none')
        }
    });
}

function send_image(img) {
    document.getElementById(img.name).value = img.src;
    $(img.id).attr('style', 'display:none');
    $('.loader').attr('style', 'display:block');
    $("#uploadForm").submit();
}

function set_example(elem, target){
    $('#'+target)[0].value = elem.text;
    $('.dropdown-content').each(function(item){
        $('.dropdown-content')[item].style.display = 'none'
    })
}

function show_examples(div_id, display){
    $('#'+div_id)[0].style.display = display
}