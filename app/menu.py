import os
import json

from app import tools as t

MENU_ITEMS = None
TYPES_MENU = None


class MenuManager:
    def __init__(self, demos, data_file = './demos_data.json'):
        global MENU_ITEMS
        global TYPES_MENU
        if MENU_ITEMS is None:
            MENU_ITEMS = self.load_menu_items_by_type(demos)
        if TYPES_MENU is None:
            TYPES_MENU = self.load_types_menu(data_file, demos)
        self.types_menu_items = TYPES_MENU
        self.menu_items_by_type = MENU_ITEMS

    def get_menu_items_by_demo(self, demo):
        for k in self.menu_items_by_type[demo.demo_type]:
            if k == demo.demo_id:
                self.menu_items_by_type[demo.demo_type][k].is_selected = True
            else:
                self.menu_items_by_type[demo.demo_type][k].is_selected = False
        return self.menu_items_by_type[demo.demo_type]

    def get_types_menu(self, demo):
        for t in self.types_menu_items:
            if demo.demo_type == t:
                self.types_menu_items[t].is_selected = True
            else:
                self.types_menu_items[t].is_selected = False
        return self.types_menu_items

    def load_types_menu(self, data_file, demos):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), data_file)
        data = {}
        menu_types = {}
        try:
            with open(path, 'r') as settings_json:
                data = json.loads(settings_json.read())
        except Exception as e:
            t.log_message('MenuManager.load_menu_types Exception on data load: ' + str(e))
            return menu_types
        for m in data['demo_types']:
            if not MENU_ITEMS.__contains__(m):
                continue
            item_data = data['demo_types'][m]
            if len(MENU_ITEMS[m].keys())>0:
                url = list(MENU_ITEMS[m].keys())[0]+'.html'
                title = item_data['title']
                menu_item = MenuItem(url=url, title=title, is_selected=False)
                menu_types[m] = menu_item
        return menu_types

    def load_menu_items_by_type(self, demos):
        menu_by_types = {}
        for demo_key in demos.demo_items:
            demo = demos.demo_items[demo_key]
            demo_type = demo.demo_type
            menu_item = MenuItem(demo.demo_id+'.html', demo.title, False)
            if menu_by_types.keys().__contains__(demo_type):
                menu_by_types[demo_type][demo.demo_id] = menu_item
            else:
                menu_by_types[demo_type] = {demo.demo_id: menu_item}
        return menu_by_types


class MenuItem:
    def __init__(self, url, title, is_selected):
        self.url = url
        self.title = title
        self.is_selected = is_selected
