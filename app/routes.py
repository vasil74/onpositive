from flask import render_template, send_from_directory, Response, jsonify
from app import app
import os
from flask import Flask, request, redirect, url_for

from app import tools as t
from app.demos import demos as d
from app import menu as m

UPLOAD_FOLDER = './app/static/data/uploads/picsart/'

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/images/<path:path>')
def send_image(path):
    return send_from_directory('static/images', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/fonts/<path:path>')
def send_font(path):
    return send_from_directory('static/fonts', path)


@app.route('/public/<path:path>')
def send_public_file(path):
    return send_from_directory('static/public', path)


@app.route('/', methods=['GET', 'POST'])
# @app.route('/index.html', methods=['GET'])  # home page disabled
def index_page():
    return demo_page(None)


@app.route('/contact.html', methods=['GET'])
def contact_page():
    return app.send_static_file('contact.html')


@app.route('/demos', methods=['GET', 'POST'])
def demos_page():
    return demo_page(None)


@app.route('/<demo_id>.html', methods=['GET', 'POST'])
def demo_page(demo_id):
    demo, ds_menu, types_menu = None, None, None
    title, demo_content = '', ''
    try:
        demos: d.Demos = d.DemoManager().demos
        if demo_id is None:
            demo_id = list(demos.demo_items.keys())[0]
        demo = demos.get_demo_by_id(demo_id)
        types_menu = m.MenuManager(demos).get_types_menu(demo)
        ds_menu = m.MenuManager(demos).get_menu_items_by_demo(demo)
        title = demo.title
        demo_content = demo.render_demo(request)
    except Exception as e:
        t.log_message('Error in: routes.py demo_page(demo_id) ' + str(demo_id) + ' \n ' + str(e) + '\n demos:' + str(
            demos) + '\n demo:' + str(demo) + '\n types_menu:' + str(types_menu) + '\n ds_menu:' + str(
            ds_menu) + '\n demo_content:' + str(demo_content))
        return app.send_static_file('404.html')
    return render_template('demo_page.html', title=title, demo=demo_content, demos_sub_menu=ds_menu,
                           types_menu=types_menu)


@app.route('/<path:filename>')
def image(filename):
    path = ''
    if filename and t.allowed_file(filename):
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    elif filename == 'favicon.ico':
        path = './app/static/favicon.ico'
    try:
        with open(path, "rb") as imageFile:
            f = imageFile.read()
            b = bytearray(f)
        return Response(b, mimetype='image/jpeg')
    except IOError:
        return app.send_static_file('404.html')
